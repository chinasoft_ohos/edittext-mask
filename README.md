# edittext-mask

#### 项目介绍
- 项目名称：edittext-mask
- 所属系列：openharmony 第三方组件适配移植
- 功能：输入框自定义component 支持内容遮罩(掩码）
- 项目移植状态：主功能完成 80%
- 调用差异：有          设置光标和设置某段文本颜色的特性尚未实现
- 开发版本：sdk6，DevEco Studio2.2 Beta1
- 基线版本：Release 1.0.5

#### 效果演示
![edittext-mask-harmony](publish/ohos-arsenal.png)
![edittext-mask-harmony](publish/README.gif)

#### 安装教程

1.在项目根目录下的build.gradle文件中,
```
allprojects {
repositories {
    maven {
        url 'https://s01.oss.sonatype.org/content/repositories/releases/'
    }
}
}
```

2.在entry模块的build.gradle文件中
```
dependencies {
    implementation 'com.gitee.chinasoft_ohos:MaskedEditText:1.0.2'
}
```

在sdk6，DevEco Studio2.2 Beta1 下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明
把这个  implementation 'com.gitee.chinasoft_ohos:MaskedEditText:0.0.1-SNAPSHOT' 加到你的 entry `build.gradle`
或者下载project并将其作为库插入.

在布局的根目录添加这行代码  xmlns:app="http://schemas.huawei.com/res/ohos/" 添加完可以使用 `app:`这个属性

      <br.com.sapereaude.maskedEditText.MaskedEditText
        ohos:id="@+id/masked"
        ohos:layout_width="match_parent"
        ohos:layout_height="40vp"
        ohos:text_input_type="pattern_number"
        app:allowed_chars="1234567890"
        app:mask="+7(###)###-##-##"
        app:hint="1234567890"
        app:keep_hint="true"
        />
		
其中mask 是你想要的输入掩码，而'#'是一个可编辑的位置(将被屏幕上的空白替换)。

你可以选择设置表示字符(如果你不想使用'#'):

    <br.com.sapereaude.maskedEditText.MaskedEditText
        ohos:layout_width="match_parent"
        ohos:layout_height="40vp"
        app:mask="ccc.ccc.ccc-cc"
        app:char_representation="c"
    />

您还可以通过编程方式更改掩码和表示字符:

	MaskedEditText editText = (MaskedEditText) findViewById(R.id.my_edit_text)
	// Setting the representation character to '$'
	editText.setCharRepresentation('$');
	// Logging the representation character
	Log.i("Representation character", editText.getCharRepresentation());
	// Setting the mask
	editText.setMask("##/##/####");
	// Logging the mask
	Log.i("Mask", editText.getMask());

启用输入软键动作(IME动作):

	<br.com.sapereaude.maskedEditText.MaskedEditText
	    ...
	    app:enable_ime_action="true"
	    ...
	/>

或通过代码设置:

	MaskedEditText editText = (MaskedEditText) findViewById(ResourceTable.Id_masked)
	editText.setImeActionEnabled(true);


#### 测试信息
CodeCheck代码测试无异常  
CloudTest代码测试无异常  
病毒安全检测通过  
当前版本demo功能与原组件基本无差异 


#### 版本迭代
* 1.0.2



