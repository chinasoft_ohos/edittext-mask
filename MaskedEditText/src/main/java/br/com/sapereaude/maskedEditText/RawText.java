/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package br.com.sapereaude.maskedEditText;

/**
 * Raw text, another words TextWithout mask characters
 *
 * @since 2021-04-12
 */
public class RawText {
    private String text;

    /**
     * 构造方法初始化text值
     */
    public RawText() {
        text = "";
    }

    /**
     * text = 012345678, range = 123 =&gt; text = 0456789
     *
     * @param range given range
     */
    public void subtractFromString(Range range) {
        String firstPart = "";
        String lastPart = "";

        if (range.getStart() > 0 && range.getStart() <= text.length()) {
            firstPart = text.substring(0, range.getStart());
        }
        if (range.getEnd() >= 0 && range.getEnd() < text.length()) {
            lastPart = text.substring(range.getEnd(), text.length());
        }
        text = firstPart.concat(lastPart);
    }

    /**
     * 添加字符串
     *
     * @param newString New String to be added
     * @param start Position to insert newString
     * @param maxLength Maximum raw text length
     * @return Number of added characters
     */
    public int addToString(String newString, int start, int maxLength) {
        String firstPart = "";
        String lastPart = "";
        String tempString = newString;
        if (tempString == null || "".equals(newString)) {
            return 0;
        } else if (start < 0) {
            return 0;
        } else if (start > text.length()) {
            return 0;
        } else {
            int count = newString.length();

            if (start > 0) {
                firstPart = text.substring(0, start);
            }
            if (start < text.length()) {
                lastPart = text.substring(start);
            }
            if (text.length() + newString.length() > maxLength) {
                count = maxLength - text.length();
                tempString = newString.substring(0, count);
            }
            text = firstPart.concat(tempString).concat(lastPart);
            return count;
        }
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    /**
     * 文本长度
     *
     * @return 返回文本长度
     */
    public int length() {
        return text.length();
    }

    /**
     * 查找文本中某个字符
     *
     * @param position 索引
     * @return 根据索引从文本中获取某字符
     */
    public char charAt(int position) {
        return text.charAt(position);
    }
}
