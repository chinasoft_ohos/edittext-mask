/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package br.com.sapereaude.maskedEditText;

import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

/**
 * 打印log
 *
 * @since 2021-04-12
 */
public class Log {
    private static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x01, "Masked");
    private static final String BLANK = " ";

    /**
     * 构造方法
     */
    private Log() {
    }

    /**
     * From: width=%d, height=%f, text=%s
     * To:width=%{public}d, height=%{public}f,text=%{public}s
     * 不支持类似 %02d 这样的补位
     *
     * @param logMessageFormat 需要传入格式
     * @return 返回替换格式
     */
    public static String replaceFormat(String logMessageFormat) {
        return logMessageFormat.replaceAll("%([d|f|s])", "%{public}$1");
    }

    /**
     * 日志打印D
     *
     * @param tag 标签
     * @param format 输入日志格式
     */
    public static void deBug(String tag, String format) {
        HiLog.debug(LABEL, tag + BLANK + replaceFormat(format));
    }

    /**
     * 日志打印i
     *
     * @param tag 标签
     * @param format 输入日志格式
     */
    public static void info(String tag, String format) {
        HiLog.info(LABEL, tag + BLANK + replaceFormat(format));
    }

    /**
     * 日志打印w
     *
     * @param tag 标签
     * @param format 输入日志格式
     */
    public static void warn(String tag, String format) {
        HiLog.warn(LABEL, tag + BLANK + replaceFormat(format));
    }

    /**
     * 日志打印e
     *
     * @param tag 标签
     * @param format 输入日志格式
     */
    public static void error(String tag, String format) {
        HiLog.error(LABEL, tag + BLANK + replaceFormat(format));
    }
}
